String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}


let app = new Vue(
    {
        el: "#app",
        data: {
            MyName: "None",
            timemode: localStorage.getItem("timemode")||"night",
            isTerminal: false,
            code: "",
            terminalValue:"",
            isNav: false,
            clipB: "",
            module: "m1.mbmproxy.com",
            ports: "1 2",
            op: "beeline",
            ips: "1.1.1.1 2.2.2.2",
            tempPort1: "532",
            tempPort2: "533",
            template1: "",
            template2: "",
            template3: "",
        },
        watch: {
            tempPort1: function(val) {
                this.rendere()
            },
            tempPort2: function(val) {
                this.rendere()
            },
            module: function(val) {
                this.rendere()
            },
            ports: function(val) {
                this.rendere()
            },
            ips: function(val) {
                this.rendere()
            },
            op: function(val) {
                this.rendere()
            }
        },
        methods: {
            ttetermina: function(value) {
    this.terminalValue = "";
    value = value.toLowerCase();
    command = value.split(" ");
    args = Object.assign([], command);
    args.splice(0,1);
    let mactions = {
        
        "help": () => { return "USE:  " + (Object.keys(mactions)).toString().split(",").join("  ")},
        
        "gay": () => { return "no u" },
        "test": () => { return "test ok" },
        "move": () => {
            for (tag of contag) {
                if (tag.name.toLowerCase().includes(args.join(" "))) {
                    this.bagtag.push(tag);
                    return "tag moved"
                }
            }
            return "not found tag" },
        "echo": () => { return args.join(" ") },
        "go": () => {  this.go(args[0]); return "traveling..." },
        "theme": () => { localStorage.setItem("timemode", localStorage.getItem("timemode") == "day" ? "night" : "day"); window.location.reload() },

        "exit": () => { this.isTerminal = false ; return "exiting..." },
        "reboot": () => { window.location.reload() ; return "rebooting..." },
        "reload": () => { window.location.reload() ; return "reloading..." },

    };
    if (mactions[command[0]]) {
        this.code = mactions[command[0]]();
    } else {
        this.code = "ERROR: " + command[0] + " not found" + "\nUSE:  " + (Object.keys(mactions)).toString().split(",").join("  ");
    };
},
focustermwin: function() {
    this.isTerminal = true;
    setTimeout(()=>{
        this.$refs.terminal.focus();
    }, 100)
},
            rendere: function() {
                server = this.module;
                this.template1 = `${this.op.toUpperCase()} ports\n\n`;
                this.template2 = `\nSOCKS5\n`;
                this.template3 = ``;
                for (ip of this.ips.split(" ")) {
                    this.template3 += `-A INPUT -p tcp -m tcp -s ${ip} -m connlimit --connlimit-above 300 -j DROP\n`;
                }
                for (port of this.ports.split(" ")) {
                    this.template1 += `http://${server}:${this.gP1(port)}\nsocks5://${server}:${this.gP2(port)}\n`;
                    this.template2 = `${server}:${this.gP1(port)}\n` + this.template2;
                    this.template2 += `${server}:${this.gP2(port)}\n`;
                    for (ip of this.ips.split(" ")) {
                        this.template3 += `-A INPUT -p tcp -m tcp --dport ${this.gP1(port)} -s ${ip} -j ACCEPT\n`;
                        this.template3 += `-A INPUT -p tcp -m tcp --dport ${this.gP2(port)} -s ${ip} -j ACCEPT\n`;
                    }
                }
                this.template2 = `${this.op.toUpperCase()} ports\n\nHTTP\n` + this.template2;
            },
            goTop: function() {
                const c = document.documentElement.scrollTop || document.body.scrollTop;
                if (c > 0) {
                    window.requestAnimationFrame(scrollToTop);
                    window.scrollTo(0, c - c / 8);
                }
            },
            gP1: function(port) {
                return this.tempPort1 + port;
            },
            gP2: function(port) {
                return this.tempPort2 + port;
            },
            go: function(where) {
                let h = window.location.host
                window.location.href = 'http://' + h + '/' + where;
            },
            cp: function(text) {
                this.clipB = text.toString();
                setTimeout(()=> {
                    let copyText = document.getElementById("clipB");
                    copyText.select();
                    document.execCommand("copy");
                }, 230)
                Toast.open(
                    {
                        position: "is-bottom-left",
                        message: "В буфере обмена",
                        queue: false,
                        duration: 2000,
                    }
                );
            },
        },
    }
);

app.rendere()